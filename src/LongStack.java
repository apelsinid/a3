import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.NoSuchElementException;

enum EExpressionElements {
   Operand, Operator
}

public class LongStack {
   private LinkedList<Long> longStack;

   public static void main (String[] argum) {
//       long result = interpret("5 2 - 7 +");
//       System.out.println(result);
      // ===== TEST LongStack.op ====
//         LongStack m1 = new LongStack();
//         m1.push (5);
//         // m1.op("*");
//         System.out.println(m1.pop());
//      interpret(null); // a) avaldis puudub - väärtuseks on null
//      interpret(""); // a) avaldis puudub - tühisõne
//      interpret("   "); // a)  avaldis puudub - whitespace-sümbolitest koosnev sõne;

//      interpret("5 2 - 7"); // b) avaldises on liiga palju arve - näit. "5 2 - 7";
//      interpret("67 xxx +"); // c) avaldises esineb lubamatu sümbol - näit. "67 xxx +";
//      interpret("3 4 + - 5"); // d) avaldises pole piisavalt arve tehte sooritamiseks - näit "3 4 + - 5".

   }

   LongStack() {
      longStack = new LinkedList<>();
   }

   public boolean stEmpty() {
      return size() == 0;
   }

   public void push (long a) {
      longStack.addLast(a);
   }

   public long pop() {
      try {
         return longStack.removeLast();
      } catch(NoSuchElementException e) {
         throw new RuntimeException("Cannot pop empty stack");
      }
   }

   public long tos() {
      try {
         return longStack.getLast();
      } catch(NoSuchElementException e) {
         throw new RuntimeException("Cannot get top of empty stack");
      }
   }

   public void op (String s) {
      try {
         long right = pop();
         long left = pop();
         push(calculate(left, right, s));
      } catch(ArithmeticException e) {
         throw new RuntimeException(e.getMessage());
      } catch(RuntimeException e) {
         throw new RuntimeException(String.format("Not enough operands for operation %s", s));
      }
   }

   private long calculate(long left, long right, String operator) {
      switch (operator) {
         case "+":
            return left + right;
         case "-":
            return left - right;
         case "*":
            return left * right;
         case "/":
            return left / right;
         default:
            throw new ArithmeticException(String.format("Unexpected operator %s", operator));
      }
   }

   private long size() {
      return longStack.size();
   }

   private long getItemByIndex(long i) {
      return longStack.get((int) i);
   }

   @Override
   public boolean equals (Object o) {
      if (!(o instanceof LongStack)) {
         return false;
      }

      LongStack otherStack = (LongStack) o;

      if (otherStack.size() - size() != 0) {
         return false;
      }

      if (otherStack.stEmpty() && stEmpty()) {
         return true;
      }

      for (int i = 0; i < otherStack.size(); i++) {
         if (otherStack.getItemByIndex(i) != getItemByIndex(i)) {
            return false;
         }
      }

      return true;
   }

   @Override
   public String toString() {
      StringBuilder output = new StringBuilder();

      for (int i = 0; i < size(); i++) {
         output.append(getItemByIndex(i));
      }

      return output.toString();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack copy = new LongStack();

      for (int i = 0; i < longStack.size(); i++) {
         copy.push(getItemByIndex(i));
      }

      if (copy.size() != longStack.size()) {
         throw new CloneNotSupportedException();
      }

      return copy;
   }

   public static long interpret (String pol) {
      Map<EExpressionElements, String> patterns = new HashMap<>();
      patterns.put(EExpressionElements.Operand, "^-?\\d*\\.?\\d+$");
      patterns.put(EExpressionElements.Operator, "[-+*/]?");

      if (pol == null || pol.trim().equals("")) {
         throw new RuntimeException(String.format("Invalid expression \"%s\"", pol));
      }

      pol = pol.trim();

      LongStack stack = new LongStack();

      String[] elements = pol.split("\\s+");

      for (int i = 0; i < elements.length; i++) {
         validate(patterns, elements[i], pol);
      }

      for (int i = 0; i < elements.length; i++) {
         String element = elements[i];
         if (element.matches(patterns.get(EExpressionElements.Operand))) {
            stack.push(Long.parseLong((element)));
         } else {
            stack.op(element);
         }
      }

      if (stack.size() > 1) {
         throw new RuntimeException(String.format("Too many operands in expression %s", pol));
      }

      return stack.pop();
   }

   private static void validate(
           Map<EExpressionElements, String> patterns,
           String operator,
           String pol) {
      if (!operator.matches(patterns.get(EExpressionElements.Operand))
              && !operator.matches(patterns.get(EExpressionElements.Operator))) {
         throw new RuntimeException(String.format("Invalid symbol \"%s\" in expression \"%s\"", operator, pol));
      }
   }

}

